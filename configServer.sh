#!/bin/bash

# Instala MySQL Server
sudo apt update
sudo apt install mysql-server -y

# Inicia el servicio de MySQL y asegúrate de que se ejecute en el inicio
sudo systemctl start mysql
sudo systemctl enable mysql

# Solicita al usuario la nueva contraseña de MySQL
read -s -p "Ingresa la nueva contraseña para el usuario 'root' de MySQL: " new_password
echo

# Configura la contraseña de root de MySQL
sudo mysql -e "ALTER USER 'root'@'%' IDENTIFIED WITH 'mysql_native_password' BY '$new_password';"

# Crea la base de datos 'chat_db' y la tabla 'chat'
sudo mysql -e "CREATE DATABASE chat_db;"
sudo mysql -e "USE chat_db; CREATE TABLE chat (id INT AUTO_INCREMENT PRIMARY KEY, username VARCHAR(255), message TEXT, timestamp TIMESTAMP);"

# Instala Python 3 y pip
sudo apt install python3 -y
sudo apt install python3-pip -y

# Instala las bibliotecas requeridas para Python
pip3 install mysql-connector-python
pip3 install tkinter

echo "Servidor de chat configurado y en funcionamiento."
